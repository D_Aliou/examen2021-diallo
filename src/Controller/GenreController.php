<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Form\GenreType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class GenreController extends AbstractController
{
    /**
     * @Route("/ajout_genre", name="genre_ajout")
     */

    //Ajouter un genre
    public function genre_ajout(Request $request, EntityManagerInterface $entityManager): Response
    {
        $genre = new Genre();       ;

        $form = $this->createForm(GenreType::class, $genre);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {           
            $genre = $form->getData();

            //$entityManager = $this->getDoctrine()->getManager($livre);
            $entityManager->persist($genre);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('Genre/addgenre.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/affiche", name="Affiche_genre")
     */

     //Affichage des livres
     public function Livres_affiche(Request $request, EntityManagerInterface $entityManager): Response
     {
         $genre = $entityManager->getRepository(Genre::class)->findAll();
         return $this->render('genre/genres.html.twig', [
             'genres' => $genre,
         ]);
     }
}