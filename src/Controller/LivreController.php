<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Form\LivreType;
//use App\Form\GenreType;
//use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LivreController extends AbstractController
{
    /**
     * @Route("/ajout_livre", name="livre_ajout")
     */
    // Ajouter un liivre
    public function livre_ajout(Request $request, EntityManagerInterface $entityManager): Response
    {
        $livre = new Livre();
        $livre->setDateAjout(new \DateTime());
        $livre->setVotes(0);

        $form = $this->createForm(LivreType::class, $livre);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {           
            $livre= $form->getData();

            //$entityManager = $this->getDoctrine()->getManager($livre);
            $entityManager->persist($livre);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('livre/addlivre.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/affiche_livre/{id}", name="livre_affiche")
     */

     //Affichage d'un livre
     public function Livre_affiche(Livre $livre, EntityManagerInterface $entityManager): Response
     {
         return $this->render('livre/livredetail.html.twig', [
             'livre' => $livre,
         ]);
     }
}
